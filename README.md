# Pitch Estimator

With model local predictions as an input, outputs .npy files with the associated fundamental frequency estimation 


positional arguments:
  file                  Folder of .preds (dictionnaries 'filename': model's
                        prediction array)

optional arguments:

  -h, --help            show this help message and exit
  
  --output OUTPUT       Output folder, (default: ./)

  --extend EXTEND       Number of preds to extend the calls on the right side
                        (default: 0)

  --pred_threshold PRED_THRESHOLD
                        Threshold for considering an prediction true (between
                        0 and 1) (default: 0.9)

  --display DISPLAY     Boolean for displaying calls and their pitch (default:
                        False)

  --saveimg SAVEIMG     Path for saving vocalisation images (default: None)

  --specific_time SPECIFIC_TIME
                        Date for running at specific time using pandas date
                        indexing, format : YYYY-MM-DD HH:MM:SS (default: None)
